//! Grit Workspace

use std::fs;
use std::path::{Path, PathBuf};

const IGNORE: &[&str] = &[
    ".",
    "..",
    ".git",
    ".real-git",
    "target",
    "Session.vim",
    "src",
];

pub struct Workspace {
    path: PathBuf,
}
#[derive(Clone)]
pub struct Entry {
    pub name: String,
    pub oid: String,
}

impl Workspace {
    pub fn new(pathname: PathBuf) -> Self {
        Self { path: pathname }
    }

    pub fn list_files(&self) -> Vec<PathBuf> {
        match fs::read_dir(&self.path) {
            Ok(entries) => entries
                .filter_map(|f| {
                    match f {
                        Ok(f) => {
                            let fname = f.file_name();
                            let fname = fname.to_str().unwrap();
                            if IGNORE.contains(&fname) {
                                None
                            } else {
                                Some(PathBuf::from(fname))
                            }
                        },
                        Err(_) => None
                    }
                })
                .collect::<Vec<PathBuf>>(),
            Err(_) => vec![],
        }
    }

    pub fn read_file(&self, path: &Path) -> std::io::Result<Vec<u8>> {
        let mut file_path = self.path.clone();
        file_path.push(path);

        fs::read(file_path)
    }
}

impl Entry {
    pub fn new(name: PathBuf, oid: String) -> Entry {
        Entry {
            name: name.as_path().to_str().unwrap().to_owned(),
            oid,
        }
    }
}
