//! Grit
//!
//! A basic port of Git to Rust
//! Built after "Building Git" by James Coglan

mod database;
mod workspace;

use database::{Blob, Database, DbObject, Tree};
use std::{env, fs, process};
use workspace::{Entry, Workspace};

fn main() {
    if let Some(command) = env::args().nth(1) {
        match command.as_str() {
            "init" => {
                let root_path = env::current_dir().unwrap().canonicalize().unwrap();

                let mut git_path = root_path.clone();
                if let Some(dir) = env::args().nth(2) {
                    git_path.push(dir);
                };
                git_path.push(".git");

                for d in &["objects", "refs"] {
                    let mut dir = git_path.clone();
                    dir.push(d);

                    if let Err(e) = fs::create_dir_all(dir) {
                        eprintln!("Error creating directory: {}", e);
                        process::exit(1);
                    }
                }

                println!(
                    "Initialised empty Grit repository in {}",
                    git_path.display()
                );
            }

            "commit" => {
                let root_path = env::current_dir().unwrap().canonicalize().unwrap();
                let mut git_path = root_path.clone();
                git_path.push(".git");
                let mut db_path = git_path.clone();
                db_path.push("objects");

                let workspace = Workspace::new(root_path);
                let database = Database::new(db_path);

                let mut entries: Vec<Entry> = Vec::new();

                for f in workspace.list_files() {
                    let path = f.as_path();
                    let data = workspace.read_file(&path).unwrap();
                    let mut blob = Blob::new(data);
                    database.store(&mut blob);

                    if let Some(oid) = blob.get_oid() {
                        entries.push(Entry::new(f, oid.to_string()));
                    }
                }

                let mut tree = Tree::new(entries);
                database.store(&mut tree);

                if let Some(oid) = tree.get_oid() {
                    println!("tree: {}", oid);
                }
            }

            other => {
                eprintln!("'{}' is not a grit command", other);
                process::exit(1);
            }
        }
        return;
    }

    eprintln!("Usage: grit COMMAND OPTIONS");
    process::exit(1);
}
