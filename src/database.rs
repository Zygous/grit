//! Grid database

use deflate::deflate_bytes_zlib;
use rand::seq::SliceRandom;
use sha1_smol::Sha1;
use std::fs;
use std::path::PathBuf;

use crate::workspace::Entry;

const TEMP_CHARS: [char; 62] = [
    'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
    'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
    'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
    '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
];
const FILE_MODE: &str = "100644";

pub trait DbObject {
    fn set_oid(&mut self, oid: Option<String>);
    fn get_oid(&self) -> &Option<String>;
    fn data(&self) -> Vec<u8>;
}

pub struct Database {
    path: PathBuf,
}
pub struct Blob {
    oid: Option<String>,
    content: Vec<u8>,
}
pub struct Tree {
    oid: Option<String>,
    entries: Vec<Entry>,
}

impl DbObject for Blob {
    fn set_oid(&mut self, oid: Option<String>) {
        self.oid = oid;
    }
    fn get_oid(&self) -> &Option<String> {
        &self.oid
    }

    fn data(&self) -> Vec<u8> {
        let mut content = Vec::from("blob".as_bytes());
        content.push(b' ');
        content.append(&mut Vec::from(self.content.len().to_string().as_bytes()));
        content.push(b'\0');
        content.append(&mut self.content.clone());

        content
    }
}
impl DbObject for Tree {
    fn set_oid(&mut self, oid: Option<String>) {
        self.oid = oid;
    }
    fn get_oid(&self) -> &Option<String> {
        &self.oid
    }

    fn data(&self) -> Vec<u8> {
        let mut buffer: Vec<u8> = Vec::new();
        for e in self.entries.iter() {
            buffer.append(&mut FILE_MODE.as_bytes().to_vec());
            buffer.push(b' ');
            buffer.append(&mut e.name.as_bytes().to_vec());
            buffer.push(b'\0');
            buffer.append(&mut hex::decode(e.oid.clone()).unwrap());
            //buffer.push(b'\0');
        };

        let mut content: Vec<u8> = "tree".as_bytes().to_vec();
        content.push(b' ');
        content.append(&mut buffer.clone().len().to_string().as_bytes().to_vec());
        content.push(b'\0');
        content.append(&mut buffer);

        content
    }
}

impl Database {
    pub fn new(pathname: PathBuf) -> Self {
        Self { path: pathname }
    }

    pub fn store(&self, obj: &mut dyn DbObject) {
        let content = obj.data();

        let oid = Sha1::from(&content).hexdigest();
        self.write_object(&oid, content);

        obj.set_oid(Some(oid));
    }

    fn write_object(&self, oid: &String, content: Vec<u8>) {
        eprintln!("Writing object with ID {}", &oid);

        let mut object_path = self.path.clone();
        object_path.push(&oid[0..2]);
        object_path.push(&oid[2..]);

        if let Some(dirname) = object_path.parent() {
            if !dirname.is_dir() {
                fs::create_dir_all(dirname).expect("Error writing object to DB");
            }

            let compressed = deflate_bytes_zlib(&content[..]);
            let temp_path = dirname.clone().join(generate_temp_name());

            fs::write(&temp_path, compressed).unwrap();
            fs::rename(temp_path, object_path).unwrap();
        } else {
            eprintln!("Error creating DB directory '{:?}' for object storage", object_path);
        }
    }
}

impl Blob {
    pub fn new(content: Vec<u8>) -> Self {
        Self { oid: None, content }
    }
}

impl Tree {
    pub fn new(mut entries: Vec<Entry>) -> Self {
        entries.sort_unstable_by(|a, b| {
            let a = &a.name;
            let b = &b.name;
            a.partial_cmp(b).unwrap_or(std::cmp::Ordering::Equal)
        });

        Tree { oid: None, entries }
    }
}

fn generate_temp_name() -> String {
    let chars: String = (1..6).map(|_| {
        TEMP_CHARS.choose(&mut rand::thread_rng()).unwrap().clone()
    }).collect();

    format!("tmp_obj_{}", chars)
}
